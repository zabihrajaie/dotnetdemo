﻿using Microsoft.AspNetCore.Mvc;

namespace DotNetDemo.Controllers
{
    public class HomeController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }
    }
}